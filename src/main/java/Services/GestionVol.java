package Services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import Entities.Vol;

@Path("/vols")
public class GestionVol {
	public static List<Vol> listeVols =new ArrayList<Vol>();
	public GestionVol() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@POST
	@Consumes("application/xml")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addEmploye(Vol vol) {
		if(listeVols.add(vol))
			return Response.status(Response.Status.CREATED).entity(listeVols).build();
		else
			return Response.status(Response.Status.NOT_FOUND).entity("404 not found").build();		
		
	}
	
	
	@GET
	@Produces("application/xml")
	public Response searchVol (@QueryParam(value="compagnie") String compagnie,@QueryParam(value="heure") String heure) {
		int index=-1;
		for (Vol i : listeVols) {
			if(i.getCompagnie().equals(compagnie)&&i.getHeure().equals(heure)) {
				index=listeVols.indexOf(i);
			}
		}
		if (index==-1)
			return  Response.status(Status.NOT_FOUND).build();
		else
		return Response.status(Status.FOUND).entity(listeVols.get(index)).build();
	}
	
	@PUT
	@Path("{numVol}")
	@Consumes(MediaType.APPLICATION_XML)
	public Response updateEmploye(@PathParam(value="numVol")String numVol,Vol e) {
	for(int i=0;i<listeVols.size();i++)
	{
		if(listeVols.get(i).getNumVol().equals("numVol")) {
			listeVols.get(i).setNumVol(e.getNumVol());
			listeVols.get(i).setCompagnie(e.getCompagnie());
			listeVols.get(i).setDestination(e.getDestination());
			listeVols.get(i).setDate(e.getDate());
			listeVols.get(i).setHeure(e.getHeure());
			return Response.status(Status.OK).entity("vol modifié").build();
		}
	}
	return Response.status(Status.NOT_MODIFIED).entity(" votre vol n'est pas modifié").build();
	}
	
	@DELETE
	@Path("{numVol}/{destination}")
	public Response DeleteeEmploye(@PathParam(value="numVol") String  numVol,@PathParam(value="destination") String  destination)
	{
		for ( int i=0;i< listeVols.size();i++ )
		{
	if(listeVols.get(i).getNumVol().equals(numVol)&&listeVols.get(i).getDestination().equals(destination))
				
			{listeVols.remove(i);
			
			return Response.status(Status.OK).entity("supp avec succes").build();
			}
			
		}return Response.status(Status.NOT_FOUND).entity("votre employe n 'a pas ete trouve").build();
	}
	
}
