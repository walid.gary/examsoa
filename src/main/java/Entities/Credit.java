package Entities;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Credit {
	
	private String referenceCredit;
	private double montant;
	private String dateDebut;
	private String dateFin;
	public String getReferenceCredit() {
		return referenceCredit;
	}
	@XmlAttribute(name="referenceCredit",required=true)
	public void setReferenceCredit(String referenceCredit) {
		this.referenceCredit = referenceCredit;
	}
	public double getMontant() {
		return montant;
	}
	@XmlElement(name="montant")
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public String getDateDebut() {
		return dateDebut;
	}
	@XmlElement(name="dateDebut")
	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}
	public String getDateFin() {
		return dateFin;
	}
	@XmlElement(name="dateFin")
	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}
	public Credit(String referenceCredit, double montant, String dateDebut, String dateFin) {
		super();
		this.referenceCredit = referenceCredit;
		this.montant = montant;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}
	public Credit() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
		result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
		long temp;
		temp = Double.doubleToLongBits(montant);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((referenceCredit == null) ? 0 : referenceCredit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Credit other = (Credit) obj;
		if (dateDebut == null) {
			if (other.dateDebut != null)
				return false;
		} else if (!dateDebut.equals(other.dateDebut))
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (Double.doubleToLongBits(montant) != Double.doubleToLongBits(other.montant))
			return false;
		if (referenceCredit == null) {
			if (other.referenceCredit != null)
				return false;
		} else if (!referenceCredit.equals(other.referenceCredit))
			return false;
		return true;
	}

	
}
