package Entities;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="infosEnfants")
public class InfoSante {

	private long age;
	private float poids;
	private float taille;
	private String typeNourriture;
	private String [] vaccins;
	private int eveil;
	
	public long getAge() {
		return age;
	}
	@XmlAttribute(name="age")
	public void setAge(long age) {
		this.age = age;
	}
	public float getPoids() {
		return poids;
	}
	@XmlElement(name="poids")
	public void setPoids(float poids) {
		this.poids = poids;
	}
	public float getTaille() {
		return taille;
	}
	@XmlElement(name="taille")
	public void setTaille(float taille) {
		this.taille = taille;
	}
	public String getTypeNourriture() {
		return typeNourriture;
	}
	@XmlElement(name="typeNourriture")
	public void setTypeNourriture(String typeNourriture) {
		this.typeNourriture = typeNourriture;
	}
	public String[] getVaccins() {
		return vaccins;
	}
	@XmlElement(name="vaccins")
	public void setVaccins(String[] vaccins) {
		this.vaccins = vaccins;
	}
	public int getEveil() {
		return eveil;
	}
	@XmlElement(name="eveil")
	public void setEveil(int eveil) {
		this.eveil = eveil;
	}
	public InfoSante(long age, float poids, float taille, String typeNourriture, String[] vaccins, int eveil) {
		super();
		this.age = age;
		this.poids = poids;
		this.taille = taille;
		this.typeNourriture = typeNourriture;
		this.vaccins = vaccins;
		this.eveil = eveil;
	}
	public InfoSante() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (age ^ (age >>> 32));
		result = prime * result + eveil;
		result = prime * result + Float.floatToIntBits(poids);
		result = prime * result + Float.floatToIntBits(taille);
		result = prime * result + ((typeNourriture == null) ? 0 : typeNourriture.hashCode());
		result = prime * result + Arrays.hashCode(vaccins);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InfoSante other = (InfoSante) obj;
		if (age != other.age)
			return false;
		if (eveil != other.eveil)
			return false;
		if (Float.floatToIntBits(poids) != Float.floatToIntBits(other.poids))
			return false;
		if (Float.floatToIntBits(taille) != Float.floatToIntBits(other.taille))
			return false;
		if (typeNourriture == null) {
			if (other.typeNourriture != null)
				return false;
		} else if (!typeNourriture.equals(other.typeNourriture))
			return false;
		if (!Arrays.equals(vaccins, other.vaccins))
			return false;
		return true;
	}
	
	
}
