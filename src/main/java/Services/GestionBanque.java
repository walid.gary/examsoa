package Services;


import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import Entities.Credit;

@Path("/Clients")
public class GestionBanque {
	static List<Credit> credits=new ArrayList<Credit>();
	public GestionBanque() {
		Credit c=new Credit("A12M5",25000,"23-11-2016","23-11-2021");
		Credit c1=new Credit("A45D6",14500,"01-01-2017","01-01-2020");
		credits.add(c);
		credits.add(c1);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response afficherTauxCredit(@QueryParam(value="anneeRemboursement")int a,@QueryParam(value="referenceCredit")String r){
		if(a==5&&r==null)
		{
				return Response.status(Status.OK).entity(5).build();
		}
		if(a==7&&r==null)
		{
				return Response.status(Status.OK).entity(6).build();
		}
		if(a==0&&r!=null)
		{
			for(Credit cr:credits){
			if(cr.getReferenceCredit().equals("A12M5"))
				return(Response.status(Status.FOUND).entity(cr)).build();
		}
		return(Response.status(Status.NOT_FOUND).entity(null)).build();
	}
	
		return(Response.status(Status.CONFLICT)).build();
	}

	
}
