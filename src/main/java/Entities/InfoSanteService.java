package Entities;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/Suivi")
public class InfoSanteService {

	public static List<InfoSante> infoSantes =new ArrayList<InfoSante>();

	public InfoSanteService() {
		
	}
	@POST
	@Consumes("application/xml")
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response addInfoSante(InfoSante infoSante) {
		if(infoSantes.add(infoSante))
			return Response.status(Response.Status.CREATED).entity(infoSantes).build();
		else
			return Response.status(Response.Status.NOT_FOUND).entity("404 not found").build();		
		
	}
	@GET
	@Produces("application/xml")
	public Response searchVol (@QueryParam(value="poids") float poids,@QueryParam(value="taille") float taille) {
		int index=-1;
		for (InfoSante i : infoSantes) {
			if(i.getPoids()==poids && i.getTaille()==taille) {
				index=infoSantes.indexOf(i);
			}
		}
		if (index==-1)
			return  Response.status(Status.NOT_FOUND).build();
		else
		return Response.status(Status.FOUND).entity(infoSantes.get(index)).build();
	}
	@GET
	@Path("/age/{age}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response RecByAge (@PathParam(value="age") long age) {
		
		for (InfoSante i : infoSantes) {
			if(i.getAge()==age) {
				System.out.println(i);
			return  Response.status(Status.OK).entity(i.getVaccins()[0]).build();
				}
			}
		
		return Response.status(Status.NOT_FOUND).build();
			}
		
}