package Entities;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Vol {
	private String numVol;
	private String compagnie;
	private String destination;
	private String date;
	private String heure;
	public String getNumVol() {
		return numVol;
	}
	@XmlAttribute(name="numVol",required=true)
	public void setNumVol(String numVol) {
		this.numVol = numVol;
	}
	public String getCompagnie() {
		return compagnie;
	}
	@XmlElement(name="compagnie")
	public void setCompagnie(String compagnie) {
		this.compagnie = compagnie;
	}
	public String getDestination() {
		return destination;
	}
	@XmlElement(name="destination")
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDate() {
		return date;
	}
	@XmlElement(name="date")
	public void setDate(String date) {
		this.date = date;
	}
	public String getHeure() {
		return heure;
	}
	@XmlElement(name="heure")
	public void setHeure(String heure) {
		this.heure = heure;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((compagnie == null) ? 0 : compagnie.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((heure == null) ? 0 : heure.hashCode());
		result = prime * result + ((numVol == null) ? 0 : numVol.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vol other = (Vol) obj;
		if (compagnie == null) {
			if (other.compagnie != null)
				return false;
		} else if (!compagnie.equals(other.compagnie))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (heure == null) {
			if (other.heure != null)
				return false;
		} else if (!heure.equals(other.heure))
			return false;
		if (numVol == null) {
			if (other.numVol != null)
				return false;
		} else if (!numVol.equals(other.numVol))
			return false;
		return true;
	}
	public Vol(String numVol, String compagnie, String destination, String date, String heure) {
		super();
		this.numVol = numVol;
		this.compagnie = compagnie;
		this.destination = destination;
		this.date = date;
		this.heure = heure;
	}
	public Vol() {
		super();
		// TODO Auto-generated constructor stub
	}

}
